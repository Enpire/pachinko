﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RingMeshCreator : MonoBehaviour
{

    public float innerRadius;
    public float outerRadius;
    public float angle;
    public int sectors;
    public float depth;

    [ContextMenu("Create mesh")]
    void CreateMesh()
    {
        var mesh = new Mesh();
        var filter = GetComponent<MeshFilter>();
        if (filter == null) filter = gameObject.AddComponent<MeshFilter>();
        filter.mesh = mesh;

        var vertices = new Vector3[sectors * 4 + 4];
        var triangles = new int[12 + 24 * sectors];

        var sectorSize = angle / sectors;
        for (var i = 0; i <= sectors; i++)
        {
            Vector3 point = new Vector3(
                (float)Math.Sin(sectorSize * i),
                (float)Math.Cos(sectorSize * i)
                );
            int vertShift = i * 4;
            vertices[vertShift + 0] = point * innerRadius;
            vertices[vertShift + 1] = point * outerRadius;
            vertices[vertShift + 2] = point * outerRadius + Vector3.back * depth;
            vertices[vertShift + 3] = point * innerRadius + Vector3.back * depth;

            if (i < sectors)
            {
                int trisShift = i * 24;
                for (var j = 0; j < 3; j++)
                {
                    triangles[trisShift + j * 6 + 0] = vertShift + j + 0;
                    triangles[trisShift + j * 6 + 1] = vertShift + j + 4;
                    triangles[trisShift + j * 6 + 2] = vertShift + j + 1;
                    triangles[trisShift + j * 6 + 3] = vertShift + j + 1;
                    triangles[trisShift + j * 6 + 4] = vertShift + j + 4;
                    triangles[trisShift + j * 6 + 5] = vertShift + j + 5;
                }
                triangles[trisShift + 3 * 6 + 0] = vertShift + 3 + 0;
                triangles[trisShift + 3 * 6 + 1] = vertShift + 3 + 4;
                triangles[trisShift + 3 * 6 + 2] = vertShift + 0 + 0;
                triangles[trisShift + 3 * 6 + 3] = vertShift + 0 + 0;
                triangles[trisShift + 3 * 6 + 4] = vertShift + 3 + 4;
                triangles[trisShift + 3 * 6 + 5] = vertShift + 0 + 4;
            }
        }
        triangles[24 * sectors + 0] = 0;
        triangles[24 * sectors + 1] = 1;
        triangles[24 * sectors + 2] = 2;
        triangles[24 * sectors + 3] = 2;
        triangles[24 * sectors + 4] = 3;
        triangles[24 * sectors + 5] = 0;
        triangles[24 * sectors + 6] = sectors * 4 + 0;
        triangles[24 * sectors + 7] = sectors * 4 + 2;
        triangles[24 * sectors + 8] = sectors * 4 + 1;
        triangles[24 * sectors + 9] = sectors * 4 + 2;
        triangles[24 * sectors + 10] = sectors * 4 + 0;
        triangles[24 * sectors + 11] = sectors * 4 + 3;

        print("Max vert index:" + triangles.Max());
        print("Vert count: " + vertices.Length);

        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.RecalculateNormals();
    }

}
