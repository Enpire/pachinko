﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launcher : MonoBehaviour {

	public Rigidbody2D ballPrefab;
	public Transform parent;
	public Vector2 direction = Vector2.up;

	public float forseMin;
	public float forseMax;
	public float forsePhase;

	public void SetForsePhase(float phase) {
		forsePhase = phase;
	}

	public void Fire() {
		float forse = forseMin + forsePhase * (forseMax - forseMin);
		// TODO pool prefabs
		Rigidbody2D ball = Instantiate (ballPrefab, transform.position, Quaternion.identity, parent);
		// TODO cache direction
		ball.AddRelativeForce(transform.rotation * direction * forse);
	}

}
