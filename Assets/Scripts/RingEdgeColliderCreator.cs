﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RingEdgeColliderCreator : MonoBehaviour
{

    public float radius;
    public float angle;
    public int sectors;

    [ContextMenu("Create edge collider")]
    void CreateEdgeCollider()
    {
        var edge = GetComponent<EdgeCollider2D>();
        if (edge == null) edge = gameObject.AddComponent<EdgeCollider2D>();

        var points = new Vector2[sectors + 1];
        var sectorSize = angle / sectors;
        for (var i = 0; i <= sectors; i++)
        {
            var point = new Vector3(
                (float)Math.Sin(sectorSize * i),
                (float)Math.Cos(sectorSize * i)
                );
            points[i] = point * radius;
        }

        edge.points = points;
    }

}
