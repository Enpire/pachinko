﻿using MessageSystem;
using UnityEngine;

public class Test : MessagingBehaviour {

    public override void OnEnable()
    {
        base.OnEnable();
        print(name + " is enabled");
    }

    [ContextMenu("Fire Event")]
    void FireEvent() {
        print (name + " firing event");
        MessageManager.Dispatch<MessagingBehaviour> (this);
    }

    [Subscribe]
    public void TestMethod(MessagingBehaviour sender) {
        print (name + " received message from " + sender.name);
    }
}
