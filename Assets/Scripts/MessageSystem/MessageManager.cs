﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace MessageSystem
{
    [AttributeUsage(AttributeTargets.Method)]
    public class Subscribe : Attribute {}

    public static class MessageManager {

        private static readonly Dictionary<Type, List<object>> Subscriptions = new Dictionary<Type, List<object>>();

        public static MethodInfo SubscribeMethod() {
            return typeof(MessageManager).GetMethod("Subscribe");
        }
        public static void Subscribe<TMessage>(Action<TMessage> callback)
        {
            if (Subscriptions.ContainsKey (typeof(TMessage))) {
                Subscriptions [typeof(TMessage)].Add (callback);
            } else {
                var list = new List<object> ();
                Subscriptions [typeof(TMessage)] = list;
                list.Add (callback);
            }

        }


        public static MethodInfo UnsubscribeMethod() {
            return typeof(MessageManager).GetMethod("Unsubscribe");
        }
        public static void Unsubscribe<TMessage>(Action<TMessage> callback) {
            if (Subscriptions.ContainsKey(typeof(TMessage))) {
                Subscriptions[typeof(TMessage)].Remove(callback);
            }
        }

        public static void Dispatch<TMessage>(TMessage msg)
        {
            if (!Subscriptions.ContainsKey (typeof(TMessage)))
                return;
            var handlers = Subscriptions [typeof(TMessage)];
            foreach (var handler in handlers) {
                ((Action<TMessage>) handler) (msg);
            }
        }

        public static object CreateDelegateByParameter(Type parameterType, object target, MethodInfo method) {

            var createDelegate = typeof(MessageManager).GetMethod("CreateDelegate")
                .MakeGenericMethod(parameterType);

            var del = createDelegate.Invoke(null, new[] { target, method });

            return del;
        }

        public static Action<TEvent> CreateDelegate<TEvent>(object target, MethodInfo method)
        {
            var del = (Action<TEvent>)Delegate.CreateDelegate(typeof(Action<TEvent>), target, method);

            return del;
        }

    }
}