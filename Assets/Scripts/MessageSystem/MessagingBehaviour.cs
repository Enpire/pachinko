﻿using System.Linq;
using UnityEngine;

namespace MessageSystem
{
    public class MessagingBehaviour : MonoBehaviour {

        public virtual void OnEnable() {
            var methods = GetType().GetMethods().Where(info => info.GetCustomAttributes(typeof(Subscribe), false).Length > 0);

            foreach (var method in methods) {
                var parameters = method.GetParameters ();
                if (parameters.Length != 1) continue;

                var parameterType = parameters [0].ParameterType;
                var subscribeMethod = MessageManager.SubscribeMethod().MakeGenericMethod (parameterType);
                var handler = MessageManager.CreateDelegateByParameter(parameterType, this, method);
                subscribeMethod.Invoke (this, new[] {handler});
            }
        }

        public virtual void OnDisable() {
            var methods = GetType().GetMethods().Where(info => info.GetCustomAttributes(typeof(Subscribe), false).Length > 0);

            foreach (var method in methods) {
                var parameters = method.GetParameters ();
                if (parameters.Length != 1) continue;

                var parameterType = parameters [0].ParameterType;
                var subscribeMethod = MessageManager.UnsubscribeMethod().MakeGenericMethod (parameterType);
                var handler = MessageManager.CreateDelegateByParameter(parameterType, this, method);
                subscribeMethod.Invoke (this, new[] {handler});
            }
        }
    }
}
