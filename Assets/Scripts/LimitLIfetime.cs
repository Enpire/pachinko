﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitLIfetime : MonoBehaviour {

	public float maxDistance;
	
	// Update is called once per frame
	void Update () {
		if (transform.position.magnitude > maxDistance) {
			Destroy (gameObject);
		}
	}
}
